import './App.css';
import PresidentialDashboard from './containers/presidentialDashboard';

function App() {
  return (
    <div className="App">
      <PresidentialDashboard />
    </div>
  );
}

export default App;
