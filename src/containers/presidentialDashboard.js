import React, { useState, useEffect } from 'react'
import {
  Row,
  Col,
  Typography,
  Statistic,
} from 'antd';

const { Title } = Typography;

function PresidentialDashboard() {
  const [results, setResults] = useState([])

  useEffect(() => {
    fetchData();
  }, [])

  const fetchData = () => {
    const url = 'https://elecciones2021back.vercel.app/api/hello'
    fetch(url, { method: 'get' }).then((data) => {
      return data.json()
    }).then((jsonData => {
      setResults(jsonData.data)
    }))
  }

  return (
      <Row type='flex'>
        {results.map((r) => {
          return (
            <Col xs={24} sm={12} md={6}>
              <Title>{r.a}</Title>
              <Statistic title="Votos" value={r.c} />
              <Statistic title="Porcentaje" value={r.d} />
            </Col>
          )
        })}
      </Row>
  )
}

export default PresidentialDashboard;
